manifesto-publish-less.pdf:

%.pdf: %.tex
	pdflatex $<
	- bibtex $*
	pdflatex $<
	pdflatex $<

clean:
	$(RM)  *.log *.aux \
	*.cfg *.glo *.idx *.toc \
	*.ilg *.ind *.out *.lof \
	*.lot *.bbl *.blg *.gls *.cut *.hd \
	*.dvi *.ps *.thm *.tgz *.zip *.rpi \
	*.pdf

open: manifesto-publish-less.pdf
	xdg-open $<

edit:
	gvim -p *.tex *.bib Makefile
	zotero &
